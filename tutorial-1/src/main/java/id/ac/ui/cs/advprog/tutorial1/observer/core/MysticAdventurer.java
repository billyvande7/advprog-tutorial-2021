package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
    }

    public void update() {
        if (!this.guild.getQuest().getTitle().equals("rumble")) getQuests().add(this.guild.getQuest());
    }

    //ToDo: Complete Me
}
