package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    public String attack() {
        return "Duar menwa";
    }
    public String getType() {
        return "Gun";
    }
}
